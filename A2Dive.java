/*
import java.util.Scanner;
public class A2Dive

	private final static int JUDGES_SIZE = 7;
	static Scanner keyboard = new Scanner(System.in);
	
	public static void main(String[] args)
		float final score is initialized by method calculateScore()
		Display Final Score	
		
	public static float inputValidScores()
		Scanner keyboard
		validScore is initialized to 0
		validScore is initialized to nextFloat/user input
		While checks if validScore is under 0 and over 10
			display: "please enter score between 0 and 10"
			validScore is initialized to nextFloat/user input
		display You have chosen score of (validScore)
		return validScore
		
	public static float inputDegreeOfDifficult()
		float degreeOfDifficulty is initialized to 0
		Display "Enter a degree of difficulty between 1.2 and 3.8"
		degreeOfDifficulty is initialized by nextFloat/user input
		While checks if degreeOfDifficulty is under 1.2 and over 3.8
			display: "please enter a degree of difficulty between 1.2 and 3.8"
			degreeOfDifficulty is initialized to nextFloat/user input
		Display "Difficulty: (degreeOfDifficulty)"
		return degreeOfDifficulty
		
	public static float [] inputAllScores()
		float [] scores is given size of JUDGES_SIZE
		Display "Enter a valid score between 0 and 10."
		for (i initialized by 0;i less than length of score array; increase i by 1)
			score[1] is initialized by method inputValidScores()
		return scores
		
	private static float calculateScore()
	{
		float [] scores2 is initialized by method inputAllScores()
		floats max initialized by 0, min initialized by 10, finalScore initialized by 0
		floats difficulty initialized by method inputDegreeOfDifficulty()		
		for (i initialized by 0;i less than length of scores2;increase i by 1)
			if (scores2[i] is higher than max)
				max initialized by scores2[i]
			if (scores2[i] is lower than min and scores2[i] greater than or equal to 0)			
				min initialized by scores2[i]
			sum is equal to sum plus scores2[2]
		finalScore is equal to ((sum-max-min)*difficulty)*.6
		return finalScore
 */
import java.util.Scanner;
public class A2Dive{
	private final static int JUDGES_SIZE = 7;
	static Scanner keyboard = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		float finalScore=calculateScore();
		System.out.println("Final score is "+finalScore);		
	}
	public static float inputValidScores()
	{
		Scanner keyboard = new Scanner(System.in);
		float validScore=0.0f;		
		validScore = keyboard.nextFloat();		
		while(validScore < 0 || validScore > 10)
		{
			System.out.println("Please enter a score between 0 and 10.");
			validScore = keyboard.nextFloat();
		}
		System.out.println("You have chosen a score of "+validScore);
		return validScore; 
	}	
	public static float inputDegreeOfDifficult()
	{
		float degreeOfDifficulty=0.0f;
		System.out.println("Enter a degree of difficulty between 1.2 and 3.8");
		degreeOfDifficulty = keyboard.nextFloat();
		while(degreeOfDifficulty < 1.2 || degreeOfDifficulty > 3.8)
		{
			System.out.println("Please enter a degree of difficulty between 1.2 and 3.8");
			degreeOfDifficulty = keyboard.nextFloat();
		}
		System.out.println("Difficulty: "+degreeOfDifficulty);
		
		return degreeOfDifficulty;
	}	
	public static float [] inputAllScores()
	{
		float [] scores = new float[JUDGES_SIZE];		
		System.out.println("Enter a valid score between 0 and 10.");		
		for(int i=0; i < scores.length; i++)
		{
			scores[i]=inputValidScores();
		}		
		/*
		for(int i=0;i<scores.length;i++)//tracing purposes
		{
			System.out.println(scores[i]);			
		}
		*/
		return scores;
	}
	private static float calculateScore()
	{
		float [] scores2 = inputAllScores();
		float max=0,min=10,sum=0,finalScore=0,difficulty=inputDegreeOfDifficult();		
		for (int i = 0;  i < scores2.length; i++)
		{
			if (scores2[i] > max)
			{
				max = scores2[i];
			}
			if (scores2[i] < min && scores2[i] >= 0)			
			{
				min = scores2[i];
			}
			sum=sum+scores2[2];
		}
		//System.out.println("Max is "+max);
		//System.out.println("Min is "+min);
		//System.out.println("Sum is "+sum);
		finalScore=((sum-max-min)*difficulty)*.6f;
		//System.out.println("Finalscore is "+finalScore);
		return finalScore;
	}
	
}